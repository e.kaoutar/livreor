let express = require('express')
let app = express()
let bodyParser = require('body-parser')
let session = require('express-session')

////////////////////////////////// Les moteurs de template /////////////////////////
//utiliser ejs pour generer les vues --> installer
app.set('view engine', 'ejs')
///////////////////////////////////////////////////////////////////////////////////



///////////////////////////////// LES USE /////////////////////////////////////////
//charger automatiquement le css qui se trouve dans le directory public -- semantic
app.use('/assets', express.static('public'))
//-----> assets permet de differencier tout ce qui est static de ce qui ne l'est pas

//middlewares : traitement de routes => body-parser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


//fonctionnalites des sessions: COPY\PASTE LA DOCUMENTATION
app.use(session({
  secret: 'hdhdhdhd',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false } //on ne traite pas la https
}))

//message d'erreur : la fonction prendra 3 params : une requete et une reponse et un paramete next (qui sert a passer au suivant)
app.use(require('./middlewares/flash')) //--> initialiser une cle flash qui va contenir l'erreur

/////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////// Les Routes //////////////////////////////////////////
//recuperer la racine et envoyer la vue index.ejs
app.get('/', (request, response) => {
    /*if(request.session.error)
    {
        response.locals.error=request.session.error
        //cette variable d'erreur je veux que tu debarasses d'elle apres
        request.session.error=undefined
    }*/ //---> cette partie est presentement gerer dans mon fichier flash.js

    //s'assurer que middleware initialise bien mes informations
    ///console.log(request.session)

    let Message = require('./models/message')
    //all est une nouvelle methode qu'on va ajouter pour affiche nos messages de la base de donnees
    Message.all((messages) => {
        response.render('pages/index', {messages : messages})
    })
})


//recuperation d'un parametre specifique
app.get('/message/:id', (request, response) => {
    //recuperation de l'id avec l'outil params
    let Message = require('./models/message')
    Message.find(request.params.id, (message) => {
        //il faut creer la methode find et puis la vue show
        response.render('messages/show', {message : message})
    })
})

app.post('/', (request, response) => {


    //Gerer le vide : on ne doit pas avoir un commentaire vide
    if(request.body.message === undefined || request.body.message === '')
    {
        //sauvegarder un message d'erreur
        request.flash('error', "Vous n'avez pas poster de messages")
        response.redirect('/')

    }
    else
    {
        let Message = require('./models/message')
        //la classe message n'existe pas presentement, il va faloire la creer dans un dossier modele pour l'utiliser apres
        Message.create(request.body.message, () =>{
        //mon message de succees
            request.flash('success', "Merci!")
            response.redirect('/')
        })
    }

    //on redirige dans chaque parce que l'action se fait de maniere asynchrone


})

//ecouter le port 80
app.listen(8080)

////////////////////////////////////////////////////////////////////////////////////////////////
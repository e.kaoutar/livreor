module.exports = function(request, response, next){

                //definir les erreurs sur le systeme lical
                if(request.session.flash)
                {
                    //tu cree une variable local qui contiendra les informations qui sont au niveau de cette session
                    response.locals.flash = request.session.flash
                    //apres tu supprimes cette session
                    request.session.flash = undefined
                }

                     request.flash= function (type, content) {
                        //cette fonction stockera dans la session ce message flash qui va contenir l'erreur (content)
                        if(request.session.flash === undefined){
                            //si cet objet n'existe pas --> on affecte le vide
                            request.session.flash ={}
                        }

                        request.session.flash[type] = content
                     }

                     //si on appelle next le processus continue sinon il reste bloque
                     next()
                 }
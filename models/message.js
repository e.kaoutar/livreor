//importer la base de donnees
let connection = require('../config/db')
let moment = require('moment')

//creation de la classe message:
class Message{

    //on va definir un constructeur ou on pourra appeler la methode moment
    constructor(row){
        //definir une propriete qui correspond a la ligne
        this.row=row
    }

    //creer des getters et des setters pour afficher correctement les enregistrements
    get content(){
        return this.row.content
    }

    get ladate(){
        return moment(this.row.ladate)
    }

    //afficher seulement le message de l'avatar sur lequel je clique
    get id(){
        return this.row.id
    }

    //definition de la methode static create : cree un enregistrement a partir du contenu du textarea
        //cette fonction on a dit qu'elle prendra un callback en second parametre
    static create(content, cb){
        connection.query('INSERT INTO message SET content=?, ladate=?', [content, new Date()], (err, result) => {

            //if(err) throw err
            //passe le resultat de la requete sql
            cb(result)

        })
    }

    //prend un callback qui sera executer quand la requete sera faite
    static all(cb){
        connection.query('SELECT * FROM message', (err, rows)=>{
            //renvoyer un message a partir de la ligne selectionnee
            cb(rows.map((row) => new Message(row)))
        })
    }

    static find(id, cb){
        connection.query('SELECT * FROM message WHERE id = ? LIMIT 1', [id], (err, rows)=>{
            //renvoyer un message a partir de la ligne selectionnee
            cb(new Message(rows[0]))
        })
    }
}

module.exports = Message
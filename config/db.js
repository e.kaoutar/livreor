let mysql      = require('mysql');
let connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'livreorr'
});

connection.connect();

//j'exporte cette connection pour pouvoir la reutiliser partout ou jaurai besoin
module.exports = connection